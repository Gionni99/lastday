package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {
    protected ArvoreBinariaPesquisa<E> pai;

    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó p deste nó.
     * @param pai O nó p.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó p deste nó.
     * @return O nó p.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
        throw new RuntimeException("Não implementado");
    }

    /**
     * Retorna o nó da árvore com o menor valor.
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo() {
        throw new RuntimeException("Não implementado");
    }

    /**
     * Retorna o nó da árvore com o maior valor.
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo() {
        throw new RuntimeException("Não implementado");
    }

    /**
     * Retorna o nó sucessor do nó especificado.
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        throw new RuntimeException("Não implementado");
    }

    /**
     * Retorna o nó predecessor do nó especificado.
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        throw new RuntimeException("Não implementado");
    }
    
    /**
     * Insere um nó na árvore.
     * @param no O nó a ser inserido.
     * @return O nó inserido
     */
    protected ArvoreBinariaPesquisa<E> insere(ArvoreBinariaPesquisa<E> no) {
        throw new RuntimeException("Não implementado");
    }
    
    /**
     * Insere um nó contendo o valor especificado na árvore.
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor) {
        return insere(new ArvoreBinariaPesquisa<>(valor));
    }

    /**
     * Exclui o nó especificado da árvore.
     * Se a raiz for excluída, retorna a nova raiz.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        throw new RuntimeException("Não implementado");
    }
}
